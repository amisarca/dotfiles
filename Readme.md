Installing on Mac:
```
ln -s ~/dotfiles/zsh-config-mac ~/.oh-my-zsh
ln -s ~/dotfiles/zshrc-mac ~/.zshrc

# Change shell to ZSH
chsh -s $(which zsh)

# Install Homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install fzf
brew install fzf
```

Installing on Linux:
```
# Install oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

cp -rs ~/dotfiles/oh-my-zsh-custom/plugins ~/.oh-my-zsh/custom/ && cp -rs ~/dotfiles/oh-my-zsh-custom/themes ~/.oh-my-zsh/custom/
ln -s ~/dotfiles/zshrc-linux ~/.zshrc
ln -s ~/dotfiles/hgrc ~/.hgrc
ln -s ~/dotfiles/blazerc ~/.blazerc
ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf

# Install the autocomplete and highlight plugins
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# Install tmp for tmux plugins
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
# Press `prefix` + I in order to load all plugins

# Install fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf 
~/.fzf/install
```