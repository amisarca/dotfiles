# Mercurial
alias hga='hg add'
alias hgc='hg commit'
alias hgb='hg book'
alias hgbk='hg bookmarks'
alias hgco='hg update'
alias hged='hg diffmerge'
# pull and update
alias hgi='hg incoming'
alias hgl='hg pull -u'
alias hglr='hg pull --rebase'
alias hgo='hg outgoing'
alias hge='hg evolve'
alias hgst='hg status'
alias hgsl='hg log --limit 20 --template "{node|short} | {date|isodatesec} | {author|user}: {desc|strip|firstline}\n" '
alias hgxl='hg xl'
alias hgca='hg commit --amend'
# list unresolved files (since hg does not list unmerged files in the status command)
alias hgun='hg resolve --list'

alias hgsync='hg pull && hg rebase -d p4head'
alias hgexport='hg fixdot && hg uploadchain'
alias hgfix='hg fixdot'
alias hgpick='hg pickcheckout'
alias hgrevert='hg revert -r p4head'

# Updates hg root
function update_hg_root() {
  local path=$(pwd)
  while [[ $path != "/" && ( ! -d "$path/.hg" ) ]]; do
    local v="$path/.."
    path=$v:A    
  done	  

  if [[ $path != "/" ]]; then
    HG_ROOT=$path 
  else
    HG_ROOT="" # hg repository not found 
  fi
}

function preexec_update_hg_root() {
    case "$2" in
        hg*)
        __EXECUTED_HG_COMMAND=1
        ;;
    esac
}

function precmd_update_hg_root() {
    if [ -n "$__EXECUTED_HG_COMMAND" ]; then
        update_hg_root
        unset __EXECUTED_HG_COMMAND
    fi
}

# Will update hg root every time user changes dir.
# This approach fast but doesn't work with some corner 
# cases:
# - user deletes .hg  directory.

# Only one function
if  [[ ${chpwd_functions[(r)update_hg_root]} != update_hg_root ]]; then
  chpwd_functions+=(update_hg_root)
fi

if [[ ${precmd_functions[(r)precmd_update_hg_root]} != precmd_update_hg_root ]]; then
  precmd_functions+=(precmd_update_hg_root)
fi

if [[ ${preexec_functions[(r)preexec_update_hg_root]} != preexec_update_hg_root ]]; then
  preexec_functions+=(preexec_update_hg_root)
fi

function hg_bookmark() {
  if [[ -n $HG_ROOT ]]; then
    local figstatus=$(hg figstatus)
    local ref=$(echo $figstatus | head -7 | tail -1) || return
    if [[ -n $ref ]]; then
      ref=$(echo "cl/$ref")
    else
      ref=$(echo $figstatus | head -9 | tail -1)
    fi
    if [[ ! -n $ref ]]; then
      ref=$(hg id -t 2> /dev/null)
    fi
    echo $ref
  fi 
}

function parse_hg_dirty() {
  if `hg figstatus | head -4 | grep -Eq "\+|\*|\-|\?"`; then
    echo "$ZSH_THEME_GIT_PROMPT_DIRTY"
  else
    echo "$ZSH_THEME_GIT_PROMPT_CLEAN"
  fi
}

function hg_prompt_info() {
    if [[ -n $HG_ROOT ]]; then
        echo "$ZSH_THEME_GIT_PROMPT_PREFIX$(hg_bookmark)$(parse_hg_dirty)$ZSH_THEME_GIT_PROMPT_SUFFIX"
    fi
}

# Default values for the appearance of the prompt.
ZSH_PROMPT_BASE_COLOR="%{${fg_bold[blue]}%}"
ZSH_THEME_HG_PROMPT_TAG="hg"
ZSH_THEME_HG_PROMPT_PREFIX=":"
ZSH_THEME_HG_PROMPT_SUFFIX=""
ZSH_THEME_HG_PROMPT_SEPARATOR="|"
ZSH_THEME_HG_PROMPT_BRANCH_COLOR="%{$fg_bold[magenta]%}"
